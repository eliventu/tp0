package ar.fiuba.tdd.tp0;

public interface Operator {
	
	public String getSymbol();
	
	public float operate(float acumResult, float operand);
	
	public int minArguments();
	
	public int maxArguments();
}