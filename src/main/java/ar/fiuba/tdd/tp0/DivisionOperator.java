package ar.fiuba.tdd.tp0;

public class DivisionOperator implements Operator {
	
	public String getSymbol() {
		return "/";
	}
	
	public float operate(float acumResult, float operand) {
		return acumResult / operand;
	}
	
	public int minArguments() {
		return 2;
	}
	
	public int maxArguments() {
		return 2;
	}
}