package ar.fiuba.tdd.tp0;

public class ModOperator implements Operator {
	
	public String getSymbol() {
		return "MOD";
	}
	
	public float operate(float acumResult, float operand) {
		return acumResult % operand;
	}
	
	public int minArguments() {
		return 2;
	}
	
	public int maxArguments() {
		return 2;
	}
}
