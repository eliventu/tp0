package ar.fiuba.tdd.tp0;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class RPNCalculator {

	private LinkedList<Operator> operators;
	
    public float eval(String expression) { 
    	createOperators();
    	CopyOnWriteArrayList<String> symbolList = getSymbolList(expression);
    	throwErrorIfInvalidSymbols(symbolList);
    	
    	while (thereAreOperators(symbolList)) {
    		LinkedList<Integer> indexList = findOperators(symbolList);
    		int currentOperatorIndex = indexList.getFirst();
    		Operator currentOperator = getOperator(symbolList.get(currentOperatorIndex));
    		CopyOnWriteArrayList<Float> operands = getOperandList(currentOperator, indexList.getFirst(), symbolList);
    		float result = getResult(operands, currentOperator);
    		int currentResultIndex = currentOperatorIndex - operands.size();
    		symbolList.add(currentResultIndex, Float.toString(result));
    	} 
    	try {
    		return Float.parseFloat(symbolList.get(0));
    	} catch (Exception e) {
    		throw new IllegalArgumentException();
    	}
    }
    
    public void createOperators() {
    	operators = new LinkedList<Operator>();
    	operators.add(new SumOperator());
    	operators.add(new MultiSumOperator());
    	operators.add(new SubstractOperator());
    	operators.add(new MultiSubstractOperator());
    	operators.add(new MultiplyOperator());
    	operators.add(new MultiMultiplyOperator());
    	operators.add(new DivisionOperator());
    	operators.add(new MultiDivisionOperator());
    	operators.add(new ModOperator());	
    }
    
    public CopyOnWriteArrayList<String> getSymbolList(String expression) throws IllegalArgumentException {
    	try {
	    	String[] symbolArray =  expression.split(" ");
	    	CopyOnWriteArrayList<String> symbolList = new CopyOnWriteArrayList<String>();
	    	for (int i = 0; i < symbolArray.length; i++) {
	    		symbolList.add(symbolArray[i]);
	    	}
	    	return symbolList;
    	} catch (Exception e) {
    		throw new IllegalArgumentException();
    	}
    }
    
    public void throwErrorIfInvalidSymbols(CopyOnWriteArrayList<String> symbolList) throws IllegalArgumentException {
    	CopyOnWriteArrayList<String> symbolListCopy = new CopyOnWriteArrayList<String>();
    	String operators = "+ - * / MOD ++ -- ** //";
    	CopyOnWriteArrayList<String> operatorsList = getSymbolList(operators);
    	symbolListCopy.addAll(symbolList);
    	symbolListCopy.removeAll(operatorsList);
    	for (int i = 0; i < symbolListCopy.size(); i++) {
    		try {
    			Integer.parseInt(symbolListCopy.get(i));
    		} catch(Exception e) {
    			throw new IllegalArgumentException();
    		}
    	}
    }
    
    public boolean thereAreOperators(CopyOnWriteArrayList<String> symbolList) {
    	return (!findOperators(symbolList).isEmpty());
    }
    
    public LinkedList<Integer> findOperators(CopyOnWriteArrayList<String> symbolList) {
    	LinkedList<Integer> indexList = new LinkedList<Integer>();
    	LinkedList<Integer> indexNotFound = new LinkedList<Integer>();
    	indexNotFound.add(-1);
    	ListIterator<Operator> it = operators.listIterator();
    	while (it.hasNext()) {
    		Operator op = it.next();
    		int index = symbolList.indexOf(op.getSymbol());
    		indexList.add(index);
    	}
    	indexList.removeAll(indexNotFound);
    	Collections.sort(indexList);
    	return indexList;    	
    }
    
    public Operator getOperator(String symbol) {
    	ListIterator<Operator> it = operators.listIterator();
    	Operator op = it.next();
    	while (!op.getSymbol().equals(symbol)) {    		
    		op = it.next();
    	}
    	return op;
    }
    
    CopyOnWriteArrayList<Float> getOperandList(Operator currentOperator, int operatorIndex, CopyOnWriteArrayList<String> symbolList) {
    	CopyOnWriteArrayList<Float> operandList = new CopyOnWriteArrayList<Float>();
    	for (int i = operatorIndex - currentOperator.maxArguments(); i < operatorIndex && operatorIndex >= currentOperator.minArguments(); i++) {
    		try {
    			String stringOperand = symbolList.get(i);
    			float floatOperand = Float.parseFloat(stringOperand);
    			operandList.add(floatOperand);
    		} catch (NumberFormatException excInvalidSymbol) {
    			throw new IllegalArgumentException();
    		} catch (IndexOutOfBoundsException excOutOfBounds) {}
    	}
    	removeOperandsAndOperator(symbolList, operatorIndex, operandList.size());
    	return operandList;
    }
    
    public void removeOperandsAndOperator(CopyOnWriteArrayList<String> symbolList, int operatorIndex, int numberOfOperands) {
    	int indexStartRemoving = operatorIndex - numberOfOperands;
    	for (int i = 0; i <= numberOfOperands; i++) {
    		symbolList.remove(indexStartRemoving);
    	}
    }
    
    public float getResult(CopyOnWriteArrayList<Float> operands, Operator operator) throws IllegalArgumentException {
    	try {
    		float result = operands.get(0);
    		for (int i = 1; i < operands.size(); i++) {
    			result = operator.operate(result, operands.get(i));
    		}
    		return result;
    	} catch (ArrayIndexOutOfBoundsException e) {
    		throw new IllegalArgumentException();
    	}    	
    }    
    
}
