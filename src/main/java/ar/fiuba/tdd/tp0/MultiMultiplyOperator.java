package ar.fiuba.tdd.tp0;

public class MultiMultiplyOperator implements Operator {
	
	public String getSymbol() {
		return "**";
	}
	
	public float operate(float acumResult, float operand) {
		return acumResult * operand;
	}
	
	public int minArguments() {
		return 1;
	}
	
	public int maxArguments() {
		return 4;
	}
}
